# ----------------------------------------------------------------------
# Makefile to pull,check and push bib files
#
# ----------------------------------------------------------------------

# Tracked bibliography files
b = bib/rmod.bib bib/others.bib

update :
	@git pull origin master

commit :
	@sh -c 'git diff --quiet $b || (git add $b && /bin/echo -n "Please enter your log message: " && read m && git commit -m "$$m" )'
	
push :
	git push origin master

#to clean citezen install
clean :
	rm -rf _pillar
	
#to install citezen 
#to do how to check that a folder exists already?
install : 
	[ -d _pillar ] || git clone https://github.com/pillar-markup/pillar.git _pillar && cd _pillar && git checkout dev-8 && chmod a+x ./scripts/build.sh && ./scripts/build.sh

html :
	./_pillar/build/pillar build html -a 

serve :
	./_pillar/build/pillar serve -w
	
openPharo :
	./_pillar/build/pharo-ui ./_pillar/build/Pharo.image
	
